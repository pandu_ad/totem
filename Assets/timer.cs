﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class timer : MonoBehaviour {

    public Text timerText;
    public float startTime;
    public bool setop;
    public Int16 counter;
    // Use this for initialization

    void Start () {
        setop = false;
	}
	
	// Update is called once per frame
	void Update () {
      
        if (setop)
        {
            timerText.text = "Time Up\nScore: "+counter;

        }
        else {
            float t = startTime - Time.time;
            string minutes = ((int)t / 60).ToString();
            string second = (t % 60).ToString("f2");

            timerText.text = minutes + ":" + second+ "\nScore: "+counter;
            if (t <= 0)
            {
                setop = true;
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoAddBlock : MonoBehaviour {
    public GameObject boxred;
    public GameObject boxgreen;
    public GameObject boxblue;

    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
       
		
	}

    public void createNewBox() {

        int i = Random.Range(1, 4);
        GameObject newObj = null;
        if (i == 1) {
            newObj = Instantiate(boxred);
        }
        else if (i == 2)
        {
            newObj = Instantiate(boxgreen);
        }
        else if (i == 3)
        {
            newObj = Instantiate(boxblue);
        }

        newObj.transform.position = new Vector3((float)-0.529, (float)5,0);
    }

    
}

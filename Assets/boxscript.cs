﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boxscript : MonoBehaviour {
    private bool isGrounded;
    public string warna;
    timer tmr;

    // Use this for initialization
    void Start () {
        tmr = GameObject.Find("Main Camera").GetComponent<timer>();
    }
	
	// Update is called once per frame
	void Update () {
        if (isGrounded && !tmr.setop)
        {
            if ((warna == "red" && Input.GetKey(KeyCode.R)) ||
                (warna == "green" && Input.GetKey(KeyCode.G)) ||
                (warna == "blue" && Input.GetKey(KeyCode.B)))
            {
                Destroy(gameObject, .2f);
                GameObject.Find("manager").GetComponent<AutoAddBlock>().createNewBox();
                isGrounded = false;
                tmr.counter += 1;
            }        
        }
    }

    void OnCollisionEnter(Collision otherObj)
    {
        if (otherObj.gameObject.name == "Plane")
        {
            isGrounded = true;
        }
    }
}
